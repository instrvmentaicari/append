Appends argument string to lines from stdin.

 echo "This is a" | append " sentence in two halves."
 ls | append " is a file in this directory."

You can also prepend by giving a 'p' character in the argument after the string.

 echo " sentence in two halves." | append "This is a"

This mode switch is case-insensitive, and allows 'a' for normal appension mode.
If you give a character which is not allowed, or 'h', the program will print usage on stderr.

Obviously, this is meant to be used in conjunction with command substitution.
For a realworld example of this, my original use case is as follows:

 while ((1)); do
  nvidia-smi -x -q|grep \<gpu_temp\>| seltok 1 \>|seltok 0 \<|append p "$(date -u|seltok 3) -- " &
  sleep 1                                # Seltok select tokens on each delimited by a given character.
 done                                    # https://gitlab.com/instrvmentaicari/seltok.git

The above example queries nvidia gpu metadata in XML format, every second, greps only the line with
 tempurature data, removes that line's XML wrapping paper then, finally, preppends the current time to
 it. The ampersand is just to get correct times as this inefficient method takes .2 to .3 seconds to
 generate and parse all that XML, which makes the time output skip seconds every 4 or 5 loop iterations.
 
The following is what that output looked like on my system when run for 10 seconds.

07:45:00 -- 55 C
07:45:01 -- 55 C
07:45:02 -- 55 C
07:45:03 -- 55 C
07:45:04 -- 55 C
07:45:05 -- 55 C
07:45:06 -- 55 C
07:45:07 -- 55 C
07:45:08 -- 55 C
07:45:09 -- 55 C
07:45:10 -- 55 C

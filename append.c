//#!/usr/bin/tcc -run
// Simply appends the token in argument 1 to every line in user input
// Second argument of letter 'p' prepends instead.
// append "some text string"
// append "some text string" p
// Reminder that text strings are represented as one argument in code.
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
char *help= "\nappend usage: append \"character string\" [mode]\n\n"
 " Description: Inserts a string into each line found on stdin, just\n"
 "  before the linebreak, and prints that line to stdout.\n\n"
 " Modes:\t(case-insensitive)\n"
 "\ta - Append. (default)\n"
 "\tp - Prepend.\n"
 "\th - Print this usage message.\n";

int main (int ζ, char **ξ){
 if (ζ<2) return 0;
 char c, r, β[1024], mode= ζ>2? ξ[2][0]: 'a'/* append mode */; // This structure leaves room for more modes to be added.
 int i= 0;
 switch (mode){
  case 'h':
  case 'H':
  default:
   write (2, help, strlen (help)); return 1;
  case 'A':
  case 'a': // Normal, append.
   while (read (0, &c, 1)){
    if (c=='\n') write (1, ξ[1], strlen (ξ[1]));
    write (1, &c, 1);
   }
   break;
  case 'P':
  case 'p': // Prepend.
   while (r= read (0, β+i, 1),c= β[i], r>0){
    if (c=='\n') write (1, ξ[1], strlen (ξ[1])), write (1, β, i+1), i= 0;
    else i++;
   }
   if (i>1&&r==0) write (1, ξ[1], strlen (ξ[1])), write (1, β, i+1),
    write (1, "\n", 1); //Handle no last line linebreak exception.
   break;
 }
 return 0;
}


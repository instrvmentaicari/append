cf=-march=native -mtune=native -O2
af=-masm=intel -S
gf=-g
cc=clang
./append:	./append.c
	$(cc) $(cf) ./append.c -o ./append 
install: ./append
	sudo cp ./append /usr/bin/
uninstall:
	sudo rm /usr/bin/append
asm:	./append.c
	$(cc) $(cf) $(af) ./append.c -o ./append.s
debug:	./append.c
	$(cc) $(cf) $(gf) ./append.c -o ./append_debug
